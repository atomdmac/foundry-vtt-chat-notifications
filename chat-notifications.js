Hooks.on("init", async () => {
  await loadTemplates([
    'modules/chat-notifications/chat-notification.html'
  ]);
});

Hooks.on("renderSidebar", async (sideBarElement) => {
  const template = await renderTemplate('modules/chat-notifications/chat-notification.html');

  // TODO: Move CSS for sidebar visibility to CSS file.
  sideBarElement.element.css({
    overflow: 'visible'
  });

  sideBarElement.element.append(template);
})

Hooks.on("renderChatMessage", async (chatMessage, html, data) => {
  if (!chatMessage.isRoll) {
    return;
  }

  // Don't display old chat messages at refresh
  if ((Date.now() - chatMessage.data['timestamp']) > 500) {
    return;
  }

  const container = $('#chat-notifications');
  const notification = $('<li></li>')
    .text(`${data.alias}: ${chatMessage.roll.total}`);

  container.prepend(notification);
  setTimeout(() =>
    notification
    .animate({opacity: 0, height: 0}, {
      done: () => notification.remove()
    }),
    5000);
})
